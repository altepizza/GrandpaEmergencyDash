#send a message to a telegram chat if the defined dash button has been pressed

from scapy.all import *
from config import *
import time
import threading
import telegram

update_id = None

def arp_display(pkt):
  if pkt[ARP].op == 1:
    if pkt[ARP].hwsrc == DASH_MAC_ADDRESS:
        for x in range(0, REPEAT_MESSAGE_N_TIMES):
            bot.send_message(CHAT_ID,text=ALARM_MESSAGE)
            time.sleep(2)

def sendHeartbeat():
    global update_id
    while True:
        for update in bot.get_updates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:  # your bot can receive updates without messages
            # Reply to the message
                update.message.reply_text('Still alive and waiting for Dash')

if __name__ == '__main__':
    bot = telegram.Bot(TELEGRAM_BOT_API_TOKEN)
    d = threading.Thread(name='daemon', target=sendHeartbeat)
    d.start()
    print 'start sniffing'
    print sniff(prn=arp_display, filter="arp", store=0)
